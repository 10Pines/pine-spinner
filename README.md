[10Pines](https://www.10pines.com/)' spinner!

![Example](https://i.imgur.com/I04jeqg.gif)

# Installation
```
npm i glowing-pine-spinner
```

# Usage
```
import GlowingPineSpinner from 'glowing-pine-spinner';

<GlowingPineSpinner 
    size={200} 
    duration={1}
    strokeWidth={10}
    firstColor={"#DBE11F"}
    secondColor={"#9FCC3B"}
    thirdColor={"#68BD44"}
    glow={true}
/>
```

| Prop        	| Type               	| Default 	|
|-------------	|--------------------	|---------	|
| size        	| Number             	| 200     	|
| duration    	| Number             	| 1       	|
| strokeWidth 	| Number             	| 10      	|
| firstColor  	| String (CSS Color) 	| #DBE11F 	|
| secondColor 	| String (CSS Color) 	| #9FCC3B 	|
| thirdColor  	| String (CSS Color) 	| #68BD44 	|
| glow        	| Bool               	| true    	|