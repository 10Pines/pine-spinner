import React from 'react';
import GlowingPineSpinner from '../lib/index';

const App = () => (
    <GlowingPineSpinner 
        size={200} 
        duration={1}
        strokeWidth={10}
        firstColor={"#DBE11F"}
        secondColor={"#9FCC3B"}
        thirdColor={"#68BD44"}
        glow={true}
    />
);

export default App;