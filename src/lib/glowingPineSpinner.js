import React from 'react';
import PropTypes from 'prop-types';
import './glowingPineSpinner.css';

class PinePolygon extends React.Component {
  render() {
    const { position, stroke, strokeWidth, animationDurationInSeconds } = this.props;

    const points = "180,0 45,270 135,270 180,360 225,270 315,270";
    const strokeLinejoin = "round";
    const strokeLinecap = "round";
    const fill = "none";

    return (
      <polygon
        className={`${position}-stroke`}
        stroke={stroke}
        points={points} 
        strokeWidth={`${strokeWidth}px`} 
        strokeLinejoin={strokeLinejoin} 
        strokeLinecap={strokeLinecap} 
        fill={fill}
        style={{ animationDuration: `${animationDurationInSeconds}s` }}
      />
    )
  }
}

export default class GlowingPineSpinner extends React.Component {
  static propTypes = {
    size: PropTypes.number,
    duration: PropTypes.number,
    strokeWidth: PropTypes.number,
    firstColor: PropTypes.string,
    secondColor: PropTypes.string,
    thirdColor: PropTypes.string,
    glow: PropTypes.bool
  };

  static defaultProps = {
    size: 200,
    duration: 1,
    strokeWidth: 10,
    firstColor: "#DBE11F",
    secondColor: "#9FCC3B",
    thirdColor: "#68BD44",
    glow: true
  };

  render() {
    const viewBox = "-20 -20 400 400";
    const { size, duration, strokeWidth, firstColor, secondColor, thirdColor, glow } = this.props;

    return (
      <div style={{ height: `${size}px`, width: `${size}px` }} className={"pine-spinner"}>
        <svg viewBox={viewBox}>
          <PinePolygon position={"first"} stroke={firstColor} strokeWidth={strokeWidth} animationDurationInSeconds={duration}/>
          <PinePolygon position={"second"} stroke={secondColor} strokeWidth={strokeWidth} animationDurationInSeconds={duration}/>
          <PinePolygon position={"third"} stroke={thirdColor} strokeWidth={strokeWidth} animationDurationInSeconds={duration}/>
        </svg>
        { 
          glow && <svg className={"blur"} viewBox={viewBox}>
            <PinePolygon position={"first"} stroke={firstColor} strokeWidth={strokeWidth} animationDurationInSeconds={duration}/>
            <PinePolygon position={"second"} stroke={secondColor} strokeWidth={strokeWidth} animationDurationInSeconds={duration}/>
            <PinePolygon position={"third"} stroke={thirdColor} strokeWidth={strokeWidth} animationDurationInSeconds={duration}/>
          </svg>
        }
      </div>
    );
  }
}